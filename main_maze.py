import os
from classe import Labyrinthe
from fonctions import mainMenu, newGameMenu

while True:
    choix1=mainMenu()

    if choix1 == "q":
        print("vous quittez le jeu...")
        break

    elif choix1 == "1":
        choix2=newGameMenu()
        lab=Labyrinthe(choix2)
        lab.jeu()

    elif choix1 == "2":
        exists = os.path.isfile("sauvegarde/Save.txt")
        if exists:
            lab=Labyrinthe("../sauvegarde/Save")
            lab.jeu()
        else:
            print("Pas de sauvegarde\nRetour au menu principal")
            break

